
make_comparative_line_charts_all_states <- function(master, quality){
  
  lowest <- master %>%
    filter(type == quality) %>%
    group_by(date, type) %>%
    slice_min(price_imputed) %>%
    ungroup()
  
  lowest_state <- sort(table(lowest$state), decreasing = TRUE)[1] |> names()
  
  highest <- master %>%
    filter(type == quality) %>%
    group_by(date, type) %>%
    slice_max(price_imputed) %>%
    ungroup()
  
  highest_state <- sort(table(highest$state), decreasing = TRUE)[1] |> names()
  
  ggplot(data = master %>% filter(type == quality)) +
    geom_line(mapping = aes(x = date, y = price_imputed, group = state, color = state)) +
    gghighlight(state %in% c("LA", highest_state, lowest_state)) +
    labs(x = NULL, y = NULL) +
    scale_y_continuous(labels = dollar_format(accuracy = 0.1))
  
}


make_comparative_line_charts_average <- function(master, quality){
  
  avg_data <- master %>%
    group_by(date, type) %>%
    summarize(avg_price = mean(price_imputed)) %>%
    ungroup()
  
  
  ggplot() +
    geom_line(
      data = master %>% filter(state == "LA", type == quality),
      mapping = aes(x = date, y = price_imputed),
      color = "red"
    ) +
    geom_line(
      data = avg_data %>% filter(type == quality),
      mapping = aes(x = date, y = avg_price)
    )
  
  
}